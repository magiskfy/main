import os, sys, json
from ui import console
from libs import environment
from libs import module
from time import sleep

default_dir = os.path.expanduser("~")
default_working_dir = os.path.join(default_dir, "Magiskfy")
config_file = os.path.join(default_dir, "magiskfy.json")

def Main():
    current_ui = console.instance(check_config())
    current_env = environment.env(current_ui.author, current_ui.working_dir)
    current_env.setup()
    keep_going = True
    while keep_going:
        saved_modules = current_env.get_saved_modules()
        selected = current_ui.get_selection(saved_modules)

        update = True
        try:
            selected = int(selected)
            if selected > len(saved_modules):
                update = True
        except Exception:
            update = False
    
        if update == True:
            selected_module_path = os.path.join(current_env.working_dir, os.path.join(current_env.modules_dir, saved_modules[selected-1]))
            module_info = get_module_info(selected_module_path)
            if not module_info == None:
                current_module = module.Module(module_info['config'][0]['id'],
                                        module_info['config'][0]['name'],
                                        module_info['config'][0]['author'],
                                        module_info['config'][0]['version'],
                                        module_info['config'][0]['version_code'],
                                        module_info['config'][0]['description'],
                                        module_info['config'][0]['replace_dir'])
                new_version = current_ui.update_module(current_module.version)
                current_module.update(new_version)
                current_ui.show_message("{} has been updated successfully to {}.\n".format(current_module.name, new_version))
            else:
                current_ui.show_error(" - The selected module info file is corrupted or doesn't exist.")
    
        if selected == "create":
            data = current_ui.create_module()
            current_module = module.Module(data[0], data[1], current_env.author, data[2], data[3], data[4], data[5])
            current_module.create()
            current_ui.show_message("{} has been created created successfully".format(current_module.name))
    
        if selected == "change_author":
            current_env.author = current_ui.change_author()
            current_env.set_settings()
            current_ui.author = current_env.author

        if selected == "exit":
            break
        

def check_config():
    try:
        with open(config_file, 'r') as config:
            data = json.load(config)
        return data
    except Exception:
        return None

def get_module_info(path):
    try:
        with open(os.path.join(path, "info.json"), 'r') as info:
            module_info = json.load(info)
        return module_info
    except Exception:
        return None

if __name__ == "__main__":
    Main()